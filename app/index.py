from flask_appbuilder import IndexView, expose
from flask import url_for, redirect


class MyIndexView(IndexView):
    @expose('/')
    def index(self):
        return redirect(url_for("HomeView.method1"))

    
