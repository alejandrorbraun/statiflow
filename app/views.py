from flask import render_template, request
from flask_appbuilder.models.sqla.interface import SQLAInterface

from flask_appbuilder.security.decorators import permission_name, has_access
from flask_appbuilder import ModelView, ModelRestApi, BaseView, expose, IndexView, SimpleFormView
from flask_appbuilder.models.sqla.filters import FilterEqualFunction
from flask_appbuilder.widgets import ListBlock, ListCarousel, ListThumbnail
from . import appbuilder, db, app
from .models import NeededProduct, Country, Producer, ProductCategory, NeededProductDefaultProfile, ProductModel, ProductType, ProducerJob
from .forms import NeededForm, ProducerForm, JobForm
from uuid import UUID
import folium
import random
from flask import g, flash, redirect
import uuid
import requests

def needed_query():
    return db.session.query(NeededProduct.item_name, NeededProduct.units_needed, # NeededProduct.item_description, 
                            NeededProduct.registration_required_to_produce, NeededProduct.point_of_contact, NeededProduct.phone, NeededProduct.email, NeededProduct.organization, NeededProduct.latitude, NeededProduct.longitude, NeededProduct.street_address, NeededProduct.city, NeededProduct.state_or_region, NeededProduct.country, NeededProduct.id, NeededProduct.status, Country).filter(NeededProduct.country_id == Country.id).filter(NeededProduct.status =="Active").all()


def producer_query():
    return db.session.query(Producer.status, Producer.capability_summary, Producer.capability_category, Producer.point_of_contact, Producer.phone, Producer.email, Producer.organization, Producer.city, Producer.state_or_region, Producer.country, Producer.latitude, Producer.longitude, Producer.id, Country).filter(Producer.country_id == Country.id).filter(Producer.status == "Available").all() # Removed street address due to privacy concerns



def get_latlng(street_address, city, state_or_region, country):
    # vals = context.get_current_parameters()
    api_key = app.config['GOOGLE_API']
    url = 'https://maps.googleapis.com/maps/api/geocode/json'
    # address = ', '.join([vals['street_address'], vals['city'], vals['state_or_region'], vals['country']])
    address = ', '.join([street_address, city, state_or_region, country])
    while True:
        r = requests.get(url, {'address': address, 'key': api_key}, timeout=5)
        if r.status_code == 200:
            break
    try:
        results = r.json()['results'][0]['geometry']['location']
        lat = results['lat']
        lng = results['lng']

    except:
        lat, lng = 0,0
    return (lat, lng)


def get_user():
    return g.user

def get_user_full_name():
    return ' '.join([g.user.first_name, g.user.last_name])

def get_user_email():
    return g.user.email


def my_producer_jobs_query():
    production_profile_ids = db.session.query(Producer).filter(Producer.created_by_fk == get_user().id).with_entities('id').all()
    return db.session.query(ProducerJob).filter(ProducerJob.production_profile_id.in_(production_profile_ids)).all()


def get_needed_item_defaults():
    cur_user = get_user()
    return db.session.query(NeededProductDefaultProfile.name, NeededProductDefaultProfile.phone, NeededProductDefaultProfile.email, NeededProductDefaultProfile.organization, NeededProductDefaultProfile.street_address, NeededProductDefaultProfile.city, NeededProductDefaultProfile.state_or_region, NeededProductDefaultProfile.country_id, NeededProductDefaultProfile.created_by_fk, Country).filter(NeededProductDefaultProfile.country_id == Country.id).filter(NeededProductDefaultProfile.created_by_fk == cur_user.id).one()



class HomeView(BaseView):
    route_base = "/"
    @expose('home')
    def method1(self):
        return render_template("home.html.jinja", base_template=appbuilder.base_template, appbuilder=appbuilder)

class CountryView(ModelView):
    datamodel = SQLAInterface(Country)
    list_widget = ListThumbnail    


class ProductCategoryView(ModelView):
    datamodel = SQLAInterface(ProductCategory)
    list_widget = ListThumbnail
    list_columns = ['name','description']
    add_columns = ['name','description']
    show_columns = ['name','description']
    edit_columns = ['name','description']    
    base_order = ('name', 'asc')

class ProductTypeView(ModelView):
    datamodel = SQLAInterface(ProductType)
    list_widget = ListThumbnail
    list_columns = ['name','product_category', 'description']
    show_columns = ['name','product_category', 'description']
    add_columns = ['name','product_category', 'description']
    edit_columns = ['name','product_category', 'description']    
    base_order = ('name', 'asc')

class ProductModelView(ModelView):
    datamodel = SQLAInterface(ProductModel)
    list_widget = ListThumbnail    
    base_permission = ['can_list', 'can_show', 'can_add']
    list_columns = ['name', 'product_type', 'description']
    show_columns = ['name', 'product_type', 'description']
    add_columns = ['name', 'product_type', 'description']
    edit_columns = ['name', 'product_type', 'description']        
    base_order = ('name', 'asc')



class JobFormView(SimpleFormView):
    form = JobForm
    message = "Job Registration Complete"
    def form_get(self,form):
        self.form_title = f"""Still needed: {request.args['still_needed']}"""
    def form_post(self, form):
        print("JOB ID: ",request.args['job_id']) # logs to console right now
        add_job = ProducerJob(related_job_id=request.args['job_id'], units_per_day= form.units_per_day.data, number_of_days=form.number_of_days.data, production_profile_id=form.production_profile.data.id)
        db.session.add(add_job)
        db.session.commit()
        flash(self.message, 'info')



class ProducerJobView(ModelView):
    datamodel = SQLAInterface(ProducerJob)
    list_widget = ListThumbnail    
    # related_views = []
    list_columns = ['producer_profile', 'status', 'units_per_day','number_of_days', 'shipment_date']
    show_columns = ['producer_profile', 'status', 'units_per_day','number_of_days', 'shipment_date']
    edit_columns = ['production_profile', 'status', 'units_per_day','number_of_days', 'shipment_date']


class AllNeededProductView(ModelView):
    datamodel = SQLAInterface(NeededProduct)
    list_widget = ListThumbnail    
    base_permissions = ['can_list', 'can_show', 'can_delete']
    list_columns = [
        "register",
        "item_name",
        "units_needed",
        "still_needed",
        "status",
        "item_description",
        "product_type",
        "registration_required_to_produce",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "latitude",
        "longitude",
        "street_address",
        "city",
        "state_or_region",
        "country"
    ]
    show_columns = [
        "register",
        "item_name",
        "units_needed",
        "still_needed",
        "status",
        "item_description",
        "product_type",
        "registration_required_to_produce",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "latitude",
        "longitude",
        "street_address",
        "city",
        "state_or_region",
        "country"
    ]

    base_order = ('status','asc')
    related_views=[ProducerJobView]
    # base_filters = [['status', FilterEqualFunction, 'Active']]



# ProducerJobView.related_views = [AllNeededProductView]
    


class NeededMapView(BaseView):
    route_base = "/"
    @expose('neededitems')
    def method1(self):
        listing_items = needed_query()
        return render_template("neededitems_map.html.jinja", base_template=appbuilder.base_template, appbuilder=appbuilder, listing_items=listing_items)



class NeededFormView(SimpleFormView):
    form = NeededForm
    form_title = 'Product Request Form'
    message = 'Product Request Submitted'

    def form_get(self, form):
        try:
            default_data = get_needed_item_defaults()
            print(default_data)
            form.point_of_contact.data = default_data.name
            form.phone.data = default_data.phone
            form.email.data = default_data.email
            form.organization.data = default_data.organization
            form.street_address.data = default_data.street_address
            form.city.data = default_data.city
            form.state_or_region.data = default_data.state_or_region
            form.country.data = default_data.country_id
        except Exception as exp:
            user = get_user()
            form.point_of_contact.data = ' '.join([user.first_name, user.last_name])
            form.email.data = user.email
    def form_post(self, form):
        lat,lng = get_latlng(form.street_address.data, form.city.data, form.state_or_region.data, form.country.data.name)
        

        product = NeededProduct(item_name = form.item_name.data, units_needed=form.units_needed.data, item_description=form.item_description.data, registration_required_to_produce=form.registration_required_to_produce.data, point_of_contact=form.point_of_contact.data, phone=form.phone.data, email=form.email.data, organization=form.organization.data, street_address=form.street_address.data, city=form.city.data, state_or_region=form.state_or_region.data, country_id = form.country.data.id, latitude=lat, longitude=lng, product_type_id = form.product_type.data.id)
        
        [product.product_model.append(i) for i in form.product_model.data]

        db.session.add(product)
        db.session.commit()
        flash(self.message, 'info')





class ProducerView(ModelView):
    datamodel=SQLAInterface(Producer)
    list_widget = ListThumbnail    
    base_permissions = ['can_edit', 'can_list', 'can_show']
    list_columns = [
        "production_profile_name",
        "status",
        "product_category",        
        "capability_summary",
        "capability_description",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "street_address",
        "city",
        "state_or_region",
        "country",
        "latitude",
        "longitude"
    ]
    
    edit_columns = [
        "status",
        "product_category",        
        "capability_summary",
        "capability_description",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "street_address",
        "city",
        "state_or_region",
        "country",
        "latitude",
        "longitude"
    ]
    add_columns = [
        "status",
        "product_category",        
        "capability_summary",
        "capability_description",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "street_address",
        "city",
        "state_or_region",
        "country",
        "latitude",
        "longitude"]
    base_filters = [["created_by", FilterEqualFunction, get_user]]

class AllProducerView(ModelView):
    datamodel = SQLAInterface(Producer)
    list_widget = ListThumbnail    
    base_permissions = ['can_list', 'can_show']
    show_columns = [
        "status",
        "capability_summary",
        "capability_description",
        "capability_category",
        "product_category",        
        "point_of_contact",
        "phone",
        "email",
        "organization",
        # "street_address",
        "city",
        "state_or_region",
        "country",
        # "latitude",
        # "longitude"
    ]
    list_columns = [
        "status",
        "capability_summary",
        "capability_description",
        "capability_category",
        "product_category",        
        "point_of_contact",
        # "phone",
        # "email",
        "organization",
        # "street_address",
        "city",
        "state_or_region",
        "country",
        # "latitude",
        # "longitude"
    ]
    base_order = ('status','asc')



class ProducerFormView(SimpleFormView):
    form = ProducerForm
    form_title = 'Production Capabilities Form'
    message = 'Production Capabilities Submitted'

    def form_get(self, form):
        form.point_of_contact.data = get_user_full_name()
        form.email.data = get_user_email()

    def form_post(self, form):
        lat,lng = get_latlng(form.street_address.data, form.city.data, form.state_or_region.data, form.country.data.name)
        lat = lat + random.uniform(.02, .03) * random.choice([-1,1])
        lng = lng + random.uniform(.02, .03) * random.choice([-1,1])
        production = Producer(status = form.status.data, capability_summary = form.capability_summary.data, capability_description = form.capability_description.data, capability_category = form.capability_category.data, point_of_contact = form.point_of_contact.data, phone = form.phone.data, email = form.email.data, organization = form.organization.data, street_address = form.street_address.data, city = form.city.data, state_or_region = form.state_or_region.data, country_id = form.country.data.id , latitude=lat, longitude = lng, product_category_id = form.product_category.data.id, production_profile_name = form.production_profile_name.data)
        db.session.add(production)
        db.session.commit()
        flash(self.message, 'info')


class ProducerMapView(BaseView):
    route_base = "/"
    @has_access
    @expose('producers')
    def method1(self):
        producer_items = producer_query()
        return render_template("producermap.html.jinja", base_template=appbuilder.base_template, appbuilder=appbuilder, producer_items=producer_items)





class NeededProductDefaultProfileView(ModelView):
    datamodel=SQLAInterface(NeededProductDefaultProfile)
    list_widget = ListThumbnail    
    list_columns = ['name','phone','email','organization','street_address','city','state_or_region','country']
    show_columns = ['name','phone','email','organization','street_address','city','state_or_region','country']
    add_columns = ['name','phone','email','organization','street_address','city','state_or_region','country']
    edit_columns = ['name','phone','email','organization','street_address','city','state_or_region','country']

    
    

class NeededProductView(ModelView):
    datamodel = SQLAInterface(NeededProduct)
    list_widget = ListThumbnail    
    base_permissions = ['can_edit', 'can_list', 'can_show']
    list_columns = [
        "status",
        "item_name",
        "units_needed",
        "item_description",
        "product_model",
        "registration_required_to_produce",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "latitude",
        "longitude",
        "street_address",
        "city",
        "state_or_region",
        "country",
        "still_needed"
    ]
    edit_columns = [
        "status",        
        "item_name",
        "units_needed",
        "item_description",
        "product_model",        
        "registration_required_to_produce",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "latitude",
        "longitude",
        "street_address",
        "city",
        "state_or_region",
        "country"
    ]

    add_columns = [
        "status",        
        "item_name",
        "units_needed",
        "item_description",
        "product_model",        
        "registration_required_to_produce",
        "point_of_contact",
        "phone",
        "email",
        "organization",
        "latitude",
        "longitude",
        "street_address",
        "city",
        "state_or_region",
        "country"]


    base_filters = [["created_by", FilterEqualFunction, get_user]]

class MyProducerJobs(ModelView):
    datamodel = SQLAInterface(ProducerJob)
    list_widget = ListThumbnail    
    list_columns = ['needed_job', 'status', 'units_per_day','number_of_days', 'shipment_date']
    show_columns = ['needed_job', 'status', 'units_per_day','number_of_days', 'shipment_date']
    edit_columns = ['status', 'units_per_day','number_of_days', 'shipment_date']    
    base_filters = [["created_by", FilterEqualFunction, get_user]]
    base_order = ('status','asc')

        
db.create_all()

appbuilder.add_view_no_menu(HomeView())
appbuilder.add_view_no_menu(NeededMapView())
appbuilder.add_view_no_menu(ProducerMapView())
# appbuilder.add_view_no_menu(ProducerJobView)




appbuilder.add_view(CountryView, "Countries", category="Backend")
appbuilder.add_view(ProductCategoryView, "Product Categories", category="Backend")
appbuilder.add_view(ProductTypeView, "Product Types", category="Backend")
appbuilder.add_view(ProductModelView, "Product Models", category="Backend")
appbuilder.add_view(ProducerJobView, "Producer Jobs", category="Backend")
appbuilder.add_view_no_menu(JobFormView)

appbuilder.add_link("Needed Items Map", "/neededitems", category="Maps", category_icon="fa-map")
appbuilder.add_link("Producer Map", "/producers", category="Maps")

appbuilder.add_view(NeededProductView, "My Products", category="Needed Products", category_icon="fas fa-plus", icon='fa-folder')
appbuilder.add_view(NeededFormView, "Add Product Request", icon="fa-plus-circle",
                     category="Needed Products")
appbuilder.add_view(NeededProductDefaultProfileView, "My Default Information", category="Needed Products", icon="fa-folder")


appbuilder.add_view(ProducerView, "My Capabilities", category="Producers", category_icon="fas fa-plus", icon='fa-folder')
appbuilder.add_view(ProducerFormView, "Add Production Capability", category="Producers", icon='fa-plus-circle')
appbuilder.add_view(MyProducerJobs, "My Current Jobs", category="Producers", icon = 'fa-clock')

appbuilder.add_view(AllProducerView, "All Producers", icon="fa-folder")
appbuilder.add_view(AllNeededProductView,"All Products", icon="fa-plus-circle")
@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )

appbuilder.security_cleanup()
