from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, ForeignKey, text, Numeric, Date, Enum, UniqueConstraint, ARRAY, Table
from sqlalchemy import func
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import uuid
import requests
from flask_appbuilder.models.mixins import AuditMixin
from flask import request
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy import func
from flask import Markup, url_for
from . import db
import operator



class Country(Model):
    __tablename__ = 'country'
    __table_args__ = {'extend_existing': True}

    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False, unique=True)

    def __repr__(self):
        return self.name


class ProductCategory(Model):
    """For: 3d Printed, Textiles, etc."""
    __tablename__ = 'product_category'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False, unique=True)
    description = Column(String(250))
    def __repr__(self):
        return self.name

    
assoc_product_model = Table('assoc_product_model', Model.metadata,
                        Column('id', UUID(as_uuid=True), primary_key=True, server_default=text("uuid_generate_v4()")),
                        Column('product_model_id', UUID(as_uuid=True), ForeignKey('product_model.id')),
                        Column('needed_product_id', UUID(as_uuid=True), ForeignKey('needed_product.id')))


assoc_product_category = Table('assoc_product_category', Model.metadata,
                                    Column('id', UUID(as_uuid=True), primary_key=True, server_default=text("uuid_generate_v4()")),
                        Column('product_category_id', UUID(as_uuid=True), ForeignKey('product_category.id')),
                        Column('product_type_id', UUID(as_uuid=True), ForeignKey('product_type.id')))


assoc_product_model_type = Table('assoc_product_model_type', Model.metadata,
                                    Column('id', UUID(as_uuid=True), primary_key=True, server_default=text("uuid_generate_v4()")),
                        Column('product_type_id', UUID(as_uuid=True), ForeignKey('product_type.id')),
                        Column('product_model_id', UUID(as_uuid=True), ForeignKey('product_model.id')))



class ProductType(Model):
    """For: Face Shield, Face Mask, Ventilator, etc."""
    __tablename__ = 'product_type'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False, unique=True)
    product_category = relationship('ProductCategory', secondary=assoc_product_category, backref='product_type')
    description = Column(String(250))

    def __repr__(self):
        return self.name

class ProductModel(Model):
    """For: 3DVerk, PRUSA Shield, etc."""

    __tablename__ = 'product_model'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False, unique=True)
    product_type = relationship("ProductType", secondary=assoc_product_model_type, backref='product_model')
    description = Column(String(250))
    def __repr__(self):
        return self.name


class Producer(AuditMixin, Model):
    __tablename__ = 'producer'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    production_profile_name = Column(String(25), nullable=False, server_default= "Profile 1")
    status = Column(Enum("Available", "Not Available", name='producer_status'), server_default = "Available")
    capability_summary = Column(String(50), nullable=False)
    capability_description = Column(String(500))
    capability_category = Column(Enum("Hobbyist", "Professional", "Commercial", name='capability_category'), server_default = "Hobbyist")
    product_category_id = Column(UUID(as_uuid=True), ForeignKey('product_category.id'), nullable=False)
    product_category = relationship("ProductCategory")
    
    point_of_contact = Column(String(50), nullable=False)
    phone = Column(String(25))
    email = Column(String(50), nullable=False)
    organization = Column(String(50), nullable=False)
    street_address = Column(String(50), nullable=False)
    city = Column(String(50), nullable=False)
    state_or_region = Column(String(50), nullable=False)
    country_id = Column(UUID(as_uuid=True), ForeignKey('country.id'), nullable=False)
    country = relationship('Country')
    latitude = Column(Numeric(8,6), nullable=False)
    longitude = Column(Numeric(9,6), nullable=False)

    def __repr__(self):
        return self.production_profile_name



class ProducerJob(AuditMixin, Model):
    __tablename__ = 'producer_job'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    related_job_id = Column(UUID(as_uuid=True), ForeignKey('needed_product.id'), nullable=False)
    related_job = relationship('NeededProduct')
    production_profile_id = Column(UUID(as_uuid=True), ForeignKey('producer.id'), nullable=False)
    production_profile = relationship('Producer')
    status = Column(Enum("In Progress", "Shipped", "Cancelled", name='job_status'), server_default = "In Progress")
    units_per_day = Column(Integer, nullable=False)
    number_of_days = Column(Integer, nullable=False)
    shipment_date = Column(Date)

    def producer_profile(self):
        # return Markup("<a href="+url_for('JobFormView.this_form_get')+"?job_id="+self.id.__str__()+">Register for Job</a>")
        return Markup("<a href="+url_for('AllProducerView.show', pk=self.production_profile_id.__str__())+">View Profile</a>")

    def needed_job(self):
        # return Markup("<a href="+url_for('AllProducerView.show', pk=self.production_profile_id.__str__())+">View Profile</a>")
        return Markup("<a href="+url_for('AllNeededProductView.show', pk = self.related_job_id.__str__())+">View Job</a>")

    def __repr__(self):
        return self.production_profile_id


# assoc_producer_job = Table('assoc_producer_job', Model.metadata,
#                            Column('id', UUID(as_uuid=True), primary_key=True, server_default=text("uuid_generate_v4()")),
#                            Column('producer_job_id', UUID(as_uuid=True), ForeignKey('producer_job.id')),
#                            Column('needed_product_id', UUID(as_uuid=True), ForeignKey('needed_product.id')))

class NeededProduct(AuditMixin, Model):
    __tablename__ = 'needed_product'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    status = Column(Enum("Active", "Received", "Cancelled", name='product_status'), server_default = "Active")
    item_name = Column(String(50), nullable=False)

    product_type_id = Column(UUID(as_uuid=True), ForeignKey('product_type.id'), nullable=False)
    product_type = relationship("ProductType")

    product_model = relationship('ProductModel', secondary=assoc_product_model, backref='needed_product')
    units_needed = Column(Integer, nullable=False)
    item_description = Column(String(750))
    registration_required_to_produce = Column(Enum('N','Y', name='registration_required'))
    point_of_contact = Column(String(50), nullable=False)
    phone = Column(String(25))
    email = Column(String(50), nullable=False)
    organization = Column(String(50), nullable=False)

    street_address = Column(String(50), nullable=False)
    city = Column(String(50), nullable=False)
    state_or_region = Column(String(50), nullable=False)
    country_id = Column(UUID(as_uuid=True), ForeignKey('country.id'), nullable=False)
    country = relationship('Country')

    latitude = Column(Numeric(8,6), nullable=False)
    longitude = Column(Numeric(9,6), nullable=False)
    


    def still_needed(self):
        jobs = db.session.query(ProducerJob).filter(ProducerJob.related_job_id == self.id).with_entities('units_per_day', 'number_of_days').all()
        still_needed = self.units_needed - sum([operator.mul(*i) for i in jobs])
        return still_needed

    def register(self):
        # return Markup("<a href="+url_for('JobFormView.this_form_get')+"?job_id="+self.id.__str__()+">Register for Job</a>")
        return Markup("<a href="+url_for('JobFormView.this_form_get')+"?job_id="+self.id.__str__()+"&still_needed="+str(self.still_needed())+">Register for Job</a>")
        

    # def __repr__(self):
    #     return f"""{self.item_name}, Registration Required? {self.registration_required_to_produce} """

class NeededProductDefaultProfile(AuditMixin, Model):
    __tablename__ = 'needed_product_default_profile'
    __table_args__ = {'extend_existing': True}
    id = Column(UUID(as_uuid=True), primary_key = True, server_default= text("uuid_generate_v4()"))
    name = Column(String(50), nullable=False)
    phone = Column(String(25))
    email = Column(String(50), nullable=False, unique=True)
    organization = Column(String(50), nullable=False)
    street_address = Column(String(50), nullable=False)
    city = Column(String(50), nullable=False)
    state_or_region = Column(String(50), nullable=False)
    country_id = Column(UUID(as_uuid=True), ForeignKey('country.id'), nullable=False)
    country = relationship('Country')


