from flask_wtf import FlaskForm
from wtforms import Form, StringField, TextAreaField, PasswordField, validators, IntegerField, SelectField, SelectMultipleField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, BS3TextAreaFieldWidget, Select2ManyWidget, Select2Widget
from flask_appbuilder.forms import DynamicForm
from wtforms.validators import DataRequired, Length
from flask import request, g
from passlib.hash import sha256_crypt
from .models import Country, ProductCategory, ProductModel, ProductType, Producer
from . import appbuilder, db, app
from functools import partial
from sqlalchemy import orm


def get_countries():
    return db.session.query(Country)

def get_product_category():
    return db.session.query(ProductCategory)

def get_product_model():
    return db.session.query(ProductModel)

def get_product_type():
    return db.session.query(ProductType)

def get_user():
    return g.user


def get_production_profile():
    return db.session.query(Producer).filter(Producer.created_by_fk == get_user().id)


class NeededForm(DynamicForm):
    item_name = StringField('Item Name',
                            validators = [DataRequired(), Length(min=1, max=50)], widget=BS3TextFieldWidget())
    units_needed = IntegerField('Units Needed', validators=[DataRequired()], widget=BS3TextFieldWidget())
    # product_category = QuerySelectField("Product Category", validators=[DataRequired()], query_factory=get_product_category, allow_blank=False, widget=Select2Widget())

    product_type = QuerySelectField("Product Type", validators=[DataRequired()], query_factory=get_product_type, allow_blank=False, widget=Select2Widget())

    product_model = QuerySelectMultipleField("Product Model(s) Accepted", query_factory=get_product_model, widget=Select2ManyWidget())

    item_description = StringField("Item Description", widget=BS3TextAreaFieldWidget(), validators=[Length(max=750, message="Must be less than 750 characters")])
    registration_required_to_produce = SelectField('Registration Required to Produce',
                                                   choices=[('N','No'), ('Y','Yes')], validators=[DataRequired()], widget=Select2Widget())
                                                   
    point_of_contact = StringField("Contact", validators=[DataRequired(), Length(min=1, max=50)], widget=BS3TextFieldWidget())
    phone = StringField("Phone", widget=BS3TextFieldWidget())
    email = StringField("Email", validators=[DataRequired()], widget=BS3TextFieldWidget())
    organization = StringField("Organization", validators=[DataRequired()], widget=BS3TextFieldWidget())
    street_address = StringField("Street Address", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    city = StringField("City", validators=[DataRequired(), Length(min=1, max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    state_or_region = StringField("State or Region", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    # country = QuerySelectField("Country", validators=[DataRequired()], query_factory=get_country_factory(['name']), get_label='name',allow_blank=False)
    country = QuerySelectField("Country", validators=[DataRequired()], query_factory=get_countries, allow_blank=False, widget=Select2Widget())
    

class ProducerForm(DynamicForm):
    production_profile_name = StringField("Production Profile Name", validators=[DataRequired(), Length(max=25, message="Must be less than 25 characters")], widget=BS3TextFieldWidget())
    status = SelectField('Status', choices=[('Available','Available'), ('Not Available','Not Available')], validators=[DataRequired()], widget=Select2Widget())
                                                   
    point_of_contact = StringField("Contact", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    product_category = QuerySelectField("Product Category", validators=[DataRequired()], query_factory=get_product_category, allow_blank=False, widget=Select2Widget())
    capability_summary = StringField("Capability Summary", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    capability_description = StringField("Capability Description", validators=[DataRequired(), Length(max=500, message="Must be less than 500 characters")], widget=BS3TextAreaFieldWidget())
    capability_category = SelectField('Capability Category',
                                      choices=[("Hobbyist","Hobbyist"), ("Professional", "Professional"), ("Commercial", "Commercial")], validators=[DataRequired()], widget=Select2Widget())
    phone = StringField("Phone", validators=[Length(max=25, message="Must be less than 25 characters")], widget=BS3TextFieldWidget())
    email = StringField("Email", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    organization = StringField("Organization", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    street_address = StringField("Street Address", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    city = StringField("City", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    state_or_region = StringField("State or Region", validators=[DataRequired(), Length(max=50, message="Must be less than 50 characters")], widget=BS3TextFieldWidget())
    # country = QuerySelectField("Country", validators=[DataRequired()], query_factory=get_country_factory(['name']), get_label='name',allow_blank=False)
    country = QuerySelectField("Country", validators=[DataRequired()], query_factory=get_countries, allow_blank=False, widget=Select2Widget())



class JobForm(DynamicForm):
    production_profile = QuerySelectField("Production Profile", validators=[DataRequired()], query_factory=get_production_profile, allow_blank=False, widget=Select2Widget())
    units_per_day = IntegerField('Units Per Day', validators=[DataRequired()], widget=BS3TextFieldWidget())
    number_of_days = IntegerField('Number of Days', validators=[DataRequired()], widget=BS3TextFieldWidget())
